## Presentation given at Nairobi LUG on 2019-07-06

* The presentation uses [Reveal.js](https://github.com/hakimel/reveal.js/)

* To view the presentation on your own computer, clone the repository and obtain 
the submodule . Go to the folder prestashop and then open index.html with
your internet browser.

* The presentation itself uses the [unlicense](https://unlicense.org/UNLICENSE), 
the included pictures and [Reveal.js](https://github.com/hakimel/reveal.js/) 
have their own licenses. 